# Laravel Programmer (Remote) · Puguh · Satrya

### Usage:
-----
#### Step 1: Get project

	git clone https://puguhjayadi@bitbucket.org/puguhjayadi/laravel_satrya.git
	
#### Step 2: Install dependencies
    composer dump-autoload
    composer install --no-scripts

#### Step 3: Create and initial populate database

*example database name = laravel_satrya*


    php artisan migrate:fresh --seed

#### Step 4: Run
    php artisan serve
    