<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
	use SoftDeletes;

	protected $table = 'employees';

	protected $fillable = [
		"full_name","nick_name","age","birth_date","address","mobile","avatar","create_by","modify_by"
	];
	protected $dates = ['deleted_at'];

}

