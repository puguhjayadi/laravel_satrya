<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Illuminate\Support\Str;
use DB;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        DB::beginTransaction();

        try {

            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required|email', 
                'password' => 'required', 
                'c_password' => 'required|same:password', 
            ]);

            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }
            
            $remember_token = Str::random(10); 
            
            $input = $request->all();

            unset($input['c_password']); 
            
            $input = array_merge($input, [
                'password' => bcrypt($input['password']), 
                'remember_token' => $remember_token
            ]);
            
            $user = User::create($input); 
            DB::commit();
            
            $response = [
                'status' => 200,
                'message' => "Success register",
                'data' => $input,
            ];
            return response()->json($response, 200); 

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }


    }
}
