<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\PasswordReset;
use App\User;
use Validator;


class ResetPasswordController extends Controller
{
	public function reset(Request $request, $token = '')
	{
		$token = Str::random(60); 

		if(empty($request->all())){
			return url('/').'/api/reset/'.$token;
		}

		$validator = Validator::make($request->all(), [ 
			'email' => 'required|email', 
			'password' => 'required', 
			'c_password' => 'required|same:password', 
		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}

		$input = $request->all();
		$email = $input['email'];
		$user = User::where('email', '=', $email)->firstOrFail();

		if (!empty($user)) {

			$user->update(['password' => bcrypt($input['password']) ]);
			PasswordReset::create(['email' => $email, 'token' => $token]);

			$response = [
				'status' => 200,
				'message' => "Password reset success",
				'data' => $user,
			];
			return response()->json($response, 200); 
		} else {
			return response()->json(['error'=> 'Failed'], 401);            
		}

	}
}
