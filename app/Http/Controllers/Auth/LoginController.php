<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use Illuminate\Support\Str;


class LoginController extends Controller
{
	public function login(Request $request)
	{
		$validator = Validator::make($request->all(), [ 
			'email' => 'required|email', 
			'password' => 'required', 
		]);

		if ($validator->fails()) { 
			return response()->json(['error'=>$validator->errors()], 401);            
		}

		$credentials = $request->only('email', 'password');

		if (Auth::attempt($credentials)) {

			$user = User::findOrFail(Auth::id());
			$remember_token = Str::random(10); 
			$user->update(['remember_token' => $remember_token]);

			$response = [
				'status' => 200,
				'message' => "Success login",
				'token' => $remember_token,
			];
			return response()->json($response, 200); 
		} else {
			return response()->json(['error'=> 'Failed'], 401);            
		}
	}

	public function reset(Request $request)
	{
		print_r($request->all());
	}

}
