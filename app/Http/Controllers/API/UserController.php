<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use Validator;


class UserController extends Controller
{

    public function index()
    {
        return User::all();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        $remember_token = Str::random(10); 
        
        $input = $request->all();

        unset($input['c_password']); 
        
        $input = array_merge($input, [
            'password' => bcrypt($input['password']), 
            'remember_token' => $remember_token
        ]);
        
        $user = User::create($input); 
        $success = [
            'token' => $remember_token,
            'name' =>  $user->name,
        ];
        return response()->json(['success'=>$success], 200); 
    }
    
    public function show($id)
    {
        return User::find($id);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        return $user;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return 204;
    }
}
