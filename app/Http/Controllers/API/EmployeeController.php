<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use App\Employee;
use Validator;
use DB;


class EmployeeController extends Controller
{

    public function list1()
    {
         /*
        http://127.0.0.1:8000/api/employees/list1?filter=
        */

        $filter    = optional(request())->filter ?? null;

        DB::beginTransaction();

        try {

            if($redis_response = Redis::get("employees_list1:filter={$filter}")){
                return response()->json( json_decode($redis_response), 200);
            }

            $employees = Employee::where('nick_name', 'LIKE', '%'.$filter.'%')
            ->orWhere('address', 'LIKE', '%'.$filter.'%')
            ->paginate();

            $response = [
                'status' => 200,
                'message' => "Success Query All Employees",
                'recordsTotal' => $employees->total(),
                'filter' => $filter,
                'data' => $employees->items(),
            ];

            DB::commit();

            Redis::set("employees_list1:filter={$filter}", json_encode($response));

            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }
    }

    public function list2()
    {
         /*
        http://127.0.0.1:8000/api/employees/list2?per_page=10&page=1&order_by=asc&sort_by=created_at&filter=
        */

        $per_page  = optional(request())->per_page ?? 1;
        $page      = optional(request())->page ?? 1;
        $sort_by   = optional(request())->sort_by ?? 'created_at';
        $order_by  = optional(request())->order_by ?? 'asc';
        $filter    = optional(request())->filter ?? null;

        DB::beginTransaction();

        try {

            if($redis_response = Redis::get("employees_list2:per_page={$per_page}:page={$page}:order_by={$order_by}:sort_by={$sort_by}:filter={$filter}")){
                return response()->json( json_decode($redis_response), 200);
            }

            $employees = Employee::where('nick_name', 'LIKE', '%'.$filter.'%')
            ->orWhere('address', 'LIKE', '%'.$filter.'%')
            ->orderBy($sort_by, $order_by)->paginate($per_page);

            $response = [
                'status' => 200,
                'message' => "Success Query All Employees",
                'total' => $employees->total(),
                'per_page' => $per_page,
                'page' => $page,
                'order_by' => $order_by,
                'sort_by' => $sort_by,
                'filter' => $filter,
                'lastPage' => $employees->lastPage(),
                'currentPage' => $employees->currentPage(),
                'data' => $employees->items(),
            ];

            DB::commit();

            Redis::set("employees_list2:per_page={$per_page}:page={$page}:order_by={$order_by}:sort_by={$sort_by}:filter={$filter}", json_encode($response));

            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            $validator = Validator::make($request->all(), [ 
                'full_name' => 'required', 
                'nick_name' => 'required', 
                'age' => 'required|integer', 
                'birth_date' => 'required|date', 
                'address' => 'required', 
                'mobile' => 'required|numeric'
            ]);

            if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
            }

            $data = $request->all();
            $data['create_by'] = Auth::user()->id;
            $data['created_at'] = date('Y-m-d H:i:s');

            Employee::create($data); 

            $response = [
                'status' => 200,
                'message' => "Employee is successfully created",
                'data' => $data,
            ];

            DB::commit();
            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }


    }


    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {

            $data = $request->all();
            $data['id'] = $id;

            $validator = Validator::make($data, [ 
                'id' => 'required|numeric|exists:employees,id',
                'full_name' => 'required', 
                'nick_name' => 'required', 
                'age' => 'required|integer', 
                'birth_date' => 'required|date', 
                'address' => 'required', 
                'mobile' => 'required|numeric'
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->messages()], 422);
            }

            $data['modify_by'] = Auth::user()->id;
            $data['updated_at'] = date('Y-m-d H:i:s');

            Employee::whereId($id)->update($data);

            $response = [
                'status' => 200,
                'message' => "Employee is successfully updated",
                'data' => $data,
            ];

            DB::commit();
            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }
    }


    public function destroy($id)
    {
        DB::beginTransaction();

        try {

            $validator = Validator::make(['employee_id' => $id], [ 
                'employee_id' => 'required|numeric|exists:employees,id',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->messages()], 422);
            }

            $employee = Employee::find($id);
            $employee->delete();

            $response = [
                'status' => 200,
                'message' => "Employee is soft deleted",
                'data' => $employee,
            ];

            DB::commit();
            return response()->json($response, 200);

        } catch(\Exception $e){

            DB::rollback();
            return response()->json(['message' => $e->getMessage() ]);
        }
    }
}
