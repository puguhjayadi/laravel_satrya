# Laravel Programmer (Remote) · Puguh · Satrya

### API documentation:
-----
#### a. User registration
**Method : GET**  
  
**Request**  
``` 
http://127.0.0.1:8000/api/register
```
**Body**
```
{
	"name": "John",
	"email": "john@mail.com", 
	"password": "password",
	"c_password": "password"
}
```
**Response**

```
{
    "status": 200,
    "message": "Success register",
    "data": {
        "name": "John",
        "email": "john@mail.com",
        "remember_token": "UyqYtYIOXI"
    }
}
```
-----
#### b. Reset password
**Method : GET**  
  
**Request**
```
http://127.0.0.1:8000/api/reset
```
**Response**
```
http://127.0.0.1:8000/api/reset/SJAmF2TPkoJhJ5Pe9yZcROXivmJUBQ8UPD3Vs50ZligyrLs5o1VAJ1hJkkih
```
**THEN**  
  
**Method : POST**
```
http://127.0.0.1:8000/api/reset/SJAmF2TPkoJhJ5Pe9yZcROXivmJUBQ8UPD3Vs50ZligyrLs5o1VAJ1hJkkih
```
**Body**
```
{
	"email": "john@mail.com", 
	"password": "password",
	"c_password": "password"
}
```
**Response**
```
{
    "status": 200,
    "message": "Password reset success",
    "data": {
        "id": 9,
        "name": "John",
        "email": "john@mail.com",
        "created_at": "2020-06-18T12:49:58.000000Z",
        "updated_at": "2020-06-18T13:04:49.000000Z"
    }
}
```
-----
#### c. User Auth
**Method : POST**  
  
**Request**
```
http://127.0.0.1:8000/api/login
```
**Body**
```
{
	"email": "john@mail.com", 
	"password": "password"
}
```
**Response**
```
{
    "status": 200,
    "message": "Success login",
    "token": "UyqYtYIOXI"
}
```
-----
#### d. Employee create
**Method : POST**  
  
**Request**
```
http://127.0.0.1:8000/api/employees
```
**Header**
```
token : UyqYtYIOXI
```
**Body**
```
{
	"full_name" : "Bayu Langit",
	"nick_name" : "Bayu",
	"age" : 1,
	"birth_date" : "1992-01-02",
	"address" : "alamat",
	"mobile" : "0812345"
}
```
**Response**
```
{
    "status": 200,
    "message": "Employee is successfully created",
    "data": {
        "full_name": "Bayu Langit",
        "nick_name": "Bayu",
        "age": 1,
        "birth_date": "1992-01-02",
        "address": "alamat",
        "mobile": "0812345",
        "create_by": 9,
        "created_at": "2020-06-18 13:11:05"
    }
}
```
-----
#### e. Employee update
**Method : PUT**  
  
**Request**
```
http://127.0.0.1:8000/api/employees/11
```
**Header**
```
token : UyqYtYIOXI
```
**Body**
```
{
	"full_name" : "Bayu Langit",
	"nick_name" : "Bayu",
	"age" : 10,
	"birth_date" : "1992-01-02",
	"address" : "alamat",
	"mobile" : "0812345"
}
```
**Response**
```
{
    "status": 200,
    "message": "Employee is successfully updated",
    "data": {
        "full_name": "Bayu Langit",
        "nick_name": "Bayu",
        "age": 10,
        "birth_date": "1992-01-02",
        "address": "alamat",
        "mobile": "0812345",
        "modify_by": 9,
        "updated_at": "2020-06-18 13:14:03"
    }
}
```
-----
#### f. Employee delete (softdelete)
**Method : DELETE**  
  
**Request**
```
http://127.0.0.1:8000/api/employees/11
```
**Header**
```
token : UyqYtYIOXI
```
**Response**
```
{
    "status": 200,
    "message": "Employee is soft deleted",
    "data": {
        "id": 11,
        "full_name": "Bayu Langit",
        "nick_name": "Bayu",
        "age": 10,
        "birth_date": "1992-01-02",
        "address": "alamat",
        "mobile": 812345,
        "avatar": null,
        "create_by": 9,
        "modify_by": 9,
        "created_at": "2020-06-18T13:11:05.000000Z",
        "updated_at": "2020-06-18T13:16:04.000000Z",
        "deleted_at": "2020-06-18T13:16:04.000000Z"
    }
}
```
-----
#### g. Employee list + filter
**Method : GET**  
  
**Request**
```
http://127.0.0.1:8000/api/employees/list1?filter=ma
```
**Header**
```
token : UyqYtYIOXI
```
**Response**
```
{
    "status": 200,
    "message": "Success Query All Employees",
    "recordsTotal": 3,
    "filter": "ma",
    "data": [
        {
            "id": 2,
            "full_name": "Maxime Stracke",
            "nick_name": "Maxime",
            "age": 5,
            "birth_date": "1997-01-17",
            "address": "61345 Cummings Manors\nWest Dakota, NM 86459",
            "mobile": 6120,
            "avatar": "m9C6hJLkWZ.png",
            "create_by": 3,
            "modify_by": 4,
            "created_at": "2020-06-18T05:55:21.000000Z",
            "updated_at": "2020-06-18T05:55:21.000000Z",
            "deleted_at": null
        },
        {
            "id": 3,
            "full_name": "Madelyn Fadel",
            "nick_name": "Madelyn",
            "age": 7,
            "birth_date": "2012-01-02",
            "address": "3903 Trantow Lights\nRutherfordton, WY 75521",
            "mobile": 53445,
            "avatar": "p7n1PXONxJ.png",
            "create_by": 4,
            "modify_by": 5,
            "created_at": "2020-06-18T05:55:21.000000Z",
            "updated_at": "2020-06-18T05:55:21.000000Z",
            "deleted_at": null
        },
        {
            "id": 5,
            "full_name": "Milton Ortiz",
            "nick_name": "Milton",
            "age": 6,
            "birth_date": "1997-10-23",
            "address": "421 Powlowski Court Apt. 051\nBraunbury, MA 04637-5463",
            "mobile": 71233,
            "avatar": "iFdEFfL1p2.png",
            "create_by": 3,
            "modify_by": 5,
            "created_at": "2020-06-18T05:55:21.000000Z",
            "updated_at": "2020-06-18T05:55:21.000000Z",
            "deleted_at": null
        }
    ]
}
```
-----
#### h. Employee list + filter + paging parameter (datatable)
**Method : GET**  
  
**Request**
```
http://127.0.0.1:8000/api/employees/list2?per_page=10&page=1&order_by=asc&sort_by=created_at&filter=ma

```
**Header**
```
token : UyqYtYIOXI
```
**Response**
```
{
    "status": 200,
    "message": "Success Query All Employees",
    "total": 3,
    "per_page": "10",
    "page": "1",
    "order_by": "asc",
    "sort_by": "created_at",
    "filter": "ma",
    "lastPage": 1,
    "currentPage": 1,
    "data": [
        {
            "id": 2,
            "full_name": "Maxime Stracke",
            "nick_name": "Maxime",
            "age": 5,
            "birth_date": "1997-01-17",
            "address": "61345 Cummings Manors\nWest Dakota, NM 86459",
            "mobile": 6120,
            "avatar": "m9C6hJLkWZ.png",
            "create_by": 3,
            "modify_by": 4,
            "created_at": "2020-06-18T05:55:21.000000Z",
            "updated_at": "2020-06-18T05:55:21.000000Z",
            "deleted_at": null
        },
        {
            "id": 3,
            "full_name": "Madelyn Fadel",
            "nick_name": "Madelyn",
            "age": 7,
            "birth_date": "2012-01-02",
            "address": "3903 Trantow Lights\nRutherfordton, WY 75521",
            "mobile": 53445,
            "avatar": "p7n1PXONxJ.png",
            "create_by": 4,
            "modify_by": 5,
            "created_at": "2020-06-18T05:55:21.000000Z",
            "updated_at": "2020-06-18T05:55:21.000000Z",
            "deleted_at": null
        },
        {
            "id": 5,
            "full_name": "Milton Ortiz",
            "nick_name": "Milton",
            "age": 6,
            "birth_date": "1997-10-23",
            "address": "421 Powlowski Court Apt. 051\nBraunbury, MA 04637-5463",
            "mobile": 71233,
            "avatar": "iFdEFfL1p2.png",
            "create_by": 3,
            "modify_by": 5,
            "created_at": "2020-06-18T05:55:21.000000Z",
            "updated_at": "2020-06-18T05:55:21.000000Z",
            "deleted_at": null
        }
    ]
}
```