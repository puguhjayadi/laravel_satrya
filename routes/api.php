<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('reset', 'Auth\ResetPasswordController@reset');
Route::post('reset/{token}', 'Auth\ResetPasswordController@reset');


Route::group(['middleware' => ['checkToken:api']], function () {
	Route::apiResource('users', 'API\UserController');
	Route::get('employees/list1', 'API\EmployeeController@list1');
	Route::get('employees/list2', 'API\EmployeeController@list2');
	Route::apiResource('employees', 'API\EmployeeController');
});



