<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {

	$firstName = $faker->firstName;
	$lastName = $faker->lastName;

	return [
		'full_name' => $firstName.' '.$lastName,
		'nick_name' => $firstName,
        'age' => $faker->randomDigit,
        'birth_date' => $faker->dateTimeBetween('1990-01-01', '2012-12-31')->format('Y-m-d'), 
        'address' => $faker->address,
        'mobile' => $faker->randomNumber(5),
        'avatar' => Str::random(10).'.png',
        'create_by' => $faker->randomElement(\App\User::pluck('id', 'id')->toArray()),
        'modify_by' => $faker->randomElement(\App\User::pluck('id', 'id')->toArray()),


    ];
});
