<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('nick_name', 100);    
            $table->integer('age');
            $table->date('birth_date');
            $table->text('address')->nullable();
            $table->integer('mobile');
            $table->string('avatar')->nullable();
            $table->bigInteger('create_by');
            $table->bigInteger('modify_by')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);   
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
